/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid;

import androidx.core.content.FileProvider;

/**
 * The type Generic file provider.
 */
public class GenericFileProvider extends FileProvider {
}
