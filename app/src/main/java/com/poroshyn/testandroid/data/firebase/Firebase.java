
/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.data.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.poroshyn.testandroid.data.DateBirthday;

import durdinapps.rxfirebase2.DataSnapshotMapper;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * The type Firebase.
 */
public class Firebase implements IFirebase {

    private DatabaseReference mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

    @Override
    public Observable<DateBirthday> getDatesList() {
        Query query = mFirebaseDatabase.child("dates");
        return RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(DateBirthday.class))
                .toObservable()
                .map(dateBirthday -> dateBirthday)
                .flatMap(Observable::fromIterable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DatabaseReference getReference() {
        return mFirebaseDatabase;
    }
}

