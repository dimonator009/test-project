/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.data.firebase;

import com.google.firebase.database.DatabaseReference;
import com.poroshyn.testandroid.data.DateBirthday;

import io.reactivex.Observable;

/**
 * The interface Firebase.
 */
public interface IFirebase {

    /**
     * Gets dates list.
     *
     * @return the dates list
     */
    Observable<DateBirthday> getDatesList();

    /**
     * Gets reference.
     *
     * @return the reference
     */
    DatabaseReference getReference();

}
