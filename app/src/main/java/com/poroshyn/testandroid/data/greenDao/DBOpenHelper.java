/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.data.greenDao;

import android.content.Context;
import android.util.Log;

import com.poroshyn.testandroid.data.DaoMaster;

import org.greenrobot.greendao.database.Database;

/**
 * The type Db open helper.
 */
public class DBOpenHelper extends DaoMaster.OpenHelper {

    /**
     * Instantiates a new Db open helper.
     *
     * @param context the context
     * @param name    the name
     */
    public DBOpenHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Log.d("DEBUG", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
    }
}
