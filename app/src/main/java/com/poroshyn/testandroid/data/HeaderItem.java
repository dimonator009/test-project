/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.data;

/**
 * The type Header item.
 */
public class HeaderItem implements RecyclerInterface {

    private String month;

    /**
     * Instantiates a new Header item.
     *
     * @param month the month
     */
    public HeaderItem(String month) {
        this.month = month;
    }

    /**
     * Gets month.
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }
}
