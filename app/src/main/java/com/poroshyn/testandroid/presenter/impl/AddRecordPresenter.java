/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.presenter.impl;

import android.util.Log;

import com.poroshyn.testandroid.data.DaoSession;
import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.data.DateBirthdayDao;
import com.poroshyn.testandroid.presenter.base.BasePresenter;
import com.poroshyn.testandroid.view.fragment.impl.AddRecordFragment;

import java.util.UUID;

/**
 * The type Add record presenter.
 */
public class AddRecordPresenter implements BasePresenter<AddRecordFragment> {

    private DaoSession daoSession;
    private AddRecordFragment addRecordFragment;
    private DateBirthdayDao dateBirthdayDao;

    /**
     * Instantiates a new Add record presenter.
     *
     * @param daoSession the dao session
     */
    public AddRecordPresenter(DaoSession daoSession) {
        this.daoSession = daoSession;
    }

    @Override
    public void setView(AddRecordFragment addRecordFragment) {
        this.addRecordFragment = addRecordFragment;
    }

    @Override
    public void unbindView() {

    }

    /**
     * Get from db date birthday.
     *
     * @param itemDatabaseId the item database id
     * @return the date birthday
     */
    public DateBirthday getFromDB(Long itemDatabaseId) {
        dateBirthdayDao = daoSession.getDateBirthdayDao();
        try {
            return dateBirthdayDao.load(itemDatabaseId);
        } catch (Exception exception) {
            Log.d("Error", "getFromDB: " + exception.getMessage());
            return null;
        }
    }

    /**
     * Add contact to db.
     *
     * @param dateBirthday the date birthday
     */
    public void addContactToDB(DateBirthday dateBirthday) {
        dateBirthdayDao = daoSession.getDateBirthdayDao();
        String uniqueID = UUID.randomUUID().toString();
        dateBirthday.setUniqueId(uniqueID);
        try {
            dateBirthdayDao.insert(dateBirthday);
            addRecordFragment.successfullyAdded();
        } catch (Exception e) {
            Log.d("TAG", "addContactToDB: " + e.getMessage());
            addRecordFragment.errorWhileAdding(dateBirthday);
        }
    }

    /**
     * Update item db.
     * (update method does not work correctly)
     *
     * @param mDateBirthday the m date birthday
     * @param itemId        the item id
     */
    public void updateItemDB(DateBirthday mDateBirthday, Long itemId) {
        dateBirthdayDao = daoSession.getDateBirthdayDao();
        try {
            dateBirthdayDao.insert(mDateBirthday);
        } catch (Exception exception) {
            Log.d("TAG", "Inert To DB Error: " + exception.getMessage());
            addRecordFragment.errorWhileSave(mDateBirthday);
            return;
        }
        try {
            dateBirthdayDao.deleteByKey(itemId);
            addRecordFragment.successfullyAdded();
        } catch (Exception exception) {
            Log.d("TAG", "Delete From DB Error: " + exception.getMessage());
            addRecordFragment.errorWhileSave(mDateBirthday);
        }
    }
}
