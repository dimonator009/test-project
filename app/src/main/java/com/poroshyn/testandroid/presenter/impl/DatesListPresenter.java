/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.presenter.impl;

import android.annotation.SuppressLint;
import android.util.Log;

import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.data.DaoSession;
import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.data.DateBirthdayDao;
import com.poroshyn.testandroid.data.HeaderItem;
import com.poroshyn.testandroid.data.RecyclerInterface;
import com.poroshyn.testandroid.presenter.base.BasePresenter;
import com.poroshyn.testandroid.view.fragment.impl.DatesListFragment;

import org.greenrobot.greendao.query.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * The type Dates list presenter.
 */
public class DatesListPresenter implements BasePresenter<DatesListFragment> {

    private DatesListFragment datesListFragment;
    private DaoSession daoSession;
    private DateBirthdayDao dateBirthdayDao;

    /**
     * Instantiates a new Dates list presenter.
     *
     * @param daoSession the dao session
     */
    public DatesListPresenter(DaoSession daoSession) {
        this.daoSession = daoSession;
    }

    @Override
    public void setView(DatesListFragment datesListFragment) {
        this.datesListFragment = datesListFragment;
    }

    /**
     * Gets dates list.
     *
     * @return the dates list
     */
    public List<RecyclerInterface> getDatesList() {
        dateBirthdayDao = daoSession.getDateBirthdayDao();
        Query<DateBirthday> contactQuery = dateBirthdayDao.queryBuilder().orderAsc(DateBirthdayDao.Properties.Name).build();
        List<DateBirthday> dateBirthdayList = contactQuery.list();
        Observable.fromIterable(dateBirthdayList).toSortedList((o1, o2) -> {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = format.parse(o1.getDate());
                date2 = format.parse(o2.getDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            assert date1 != null;
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);
            cal1.set(Calendar.YEAR, 2000);

            assert date2 != null;
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);
            cal2.set(Calendar.YEAR, 2000);

            return -1 * cal2.getTime().compareTo(cal1.getTime());

        }).subscribe(dateBirthdays -> {
            dateBirthdayList.clear();
            dateBirthdayList.addAll(dateBirthdays);
            dateBirthdayDao.updateInTx(dateBirthdays);
        }, throwable -> {
        }).dispose();

        List<RecyclerInterface> recyclerInterfaces = new ArrayList<>();

        int mounth = -1;

        for (int i = 0; i < dateBirthdayList.size(); i++) {
            DateBirthday dateBirthday = dateBirthdayList.get(i);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date date = null;

            try {
                date = format.parse(dateBirthday.getDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            assert date != null;
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            if (mounth == -1) {
                mounth = cal.get(Calendar.MONTH);
                recyclerInterfaces.add(new HeaderItem(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
                recyclerInterfaces.add(dateBirthday);
            } else if (mounth != cal.get(Calendar.MONTH)) {
                mounth = cal.get(Calendar.MONTH);
                recyclerInterfaces.add(new HeaderItem(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
                recyclerInterfaces.add(dateBirthday);
            } else {
                recyclerInterfaces.add(dateBirthday);
            }
        }

        return recyclerInterfaces;
    }

    /**
     * Delete item.
     *
     * @param dateBirthday the date birthday
     * @param position     the position
     */
    public void deleteItem(DateBirthday dateBirthday, int position) {
        try {
            dateBirthdayDao.delete(dateBirthday);
            datesListFragment.deleteItem(position);
        } catch (Exception e) {
            datesListFragment.snackBarWithoutAction(datesListFragment.getString(R.string.delete_error));
            Log.d(TAG, "Delete item Error: " + e.getMessage());
        }
    }

    @Override
    public void unbindView() {

    }
}
