/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.presenter.base;

import androidx.fragment.app.Fragment;

/**
 * The interface Base presenter.
 *
 * @param <T> the type parameter
 */
public interface BasePresenter<T extends Fragment> {
    /**
     * Sets view.
     *
     * @param t the t
     */
    void setView(T t);

    /**
     * Unbind view.
     */
    void unbindView();
}
