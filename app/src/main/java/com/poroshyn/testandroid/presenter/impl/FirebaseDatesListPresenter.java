/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.presenter.impl;

import android.annotation.SuppressLint;
import android.util.Log;

import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.data.DaoSession;
import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.data.DateBirthdayDao;
import com.poroshyn.testandroid.data.HeaderItem;
import com.poroshyn.testandroid.data.RecyclerInterface;
import com.poroshyn.testandroid.data.firebase.IFirebase;
import com.poroshyn.testandroid.data.rxjava.DisposableManager;
import com.poroshyn.testandroid.presenter.base.BasePresenter;
import com.poroshyn.testandroid.view.fragment.impl.FirebaseDatesListFragment;

import org.greenrobot.greendao.query.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * The type Firebase dates list presenter.
 */
public class FirebaseDatesListPresenter implements BasePresenter<FirebaseDatesListFragment> {

    private FirebaseDatesListFragment firebaseDatesListFragment;
    private IFirebase firebase;
    private DaoSession daoSession;
    private DateBirthdayDao dateBirthdayDao;
    private Query<DateBirthday> contactQuery;

    /**
     * Instantiates a new Firebase dates list presenter.
     *
     * @param firebase   the firebase
     * @param daoSession the dao session
     */
    public FirebaseDatesListPresenter(IFirebase firebase, DaoSession daoSession) {
        this.firebase = firebase;
        this.daoSession = daoSession;
    }

    @Override
    public void setView(FirebaseDatesListFragment firebaseDatesListFragment) {
        this.firebaseDatesListFragment = firebaseDatesListFragment;
        dateBirthdayDao = daoSession.getDateBirthdayDao();
        contactQuery = dateBirthdayDao.queryBuilder().orderAsc(DateBirthdayDao.Properties.Name).build();
    }

    @Override
    public void unbindView() {
        DisposableManager.dispose();
    }

    /**
     * Gets dates list firebase.
     */
    public void getDatesListFirebase() {
        List<DateBirthday> firebaseListDates = new ArrayList<>();
        DisposableManager.add(firebase.getDatesList()
                .timeout(10, TimeUnit.SECONDS, observer ->
                        firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.timeout)))
                .doOnError(dateBirthdayNotification -> firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.refresh_error)))
                .doOnComplete(() -> synchronizeLists(firebaseListDates))
                .subscribe(firebaseListDates::add, throwable -> Log.d(TAG, "getDatesListFirebase: " + throwable.getMessage())));
    }

    /**
     * Upload items to firebase
     *
     * @param firebaseListDates is list that upload to firebase
     */
    private void uploadToFirebase(List<DateBirthday> firebaseListDates) {
        Map<String, Object> update = new HashMap<>();
        Observable.fromIterable(firebaseListDates).doOnError(throwable -> {
            firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.refresh_error));
            Log.d(TAG, "uploadToFirebase error: " + throwable.getMessage());
        }).forEach(dateBirthday -> update.put(dateBirthday.getUniqueId(), dateBirthday)).dispose();
        firebase.getReference().child("dates").setValue(update).addOnCompleteListener(task -> firebaseDatesListFragment.addDatesList());
    }

    /**
     * Add items to firebase list
     *
     * @param firebaseListDates is list from firebase
     */
    private void checkLocalList(List<DateBirthday> firebaseListDates) {
        List<DateBirthday> localListDates = contactQuery.list();
        DisposableManager.add(Observable.fromIterable(localListDates).doOnError(throwable -> {
            firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.refresh_error));
            Log.d(TAG, "Add to Firebase Error: " + throwable.getMessage());
        }).doOnComplete(() -> uploadToFirebase(firebaseListDates)).forEach(localDate -> {
            boolean uploadToFirebase = true;
            for (int i = 0; i < firebaseListDates.size(); i++) {
                firebaseListDates.get(i).setId(null);
                DateBirthday firebaseDate = firebaseListDates.get(i);
                if (localDate.getUniqueId().equals(firebaseDate.getUniqueId())) {
                    uploadToFirebase = false;
                    break;
                }
            }
            if (uploadToFirebase) {
                DateBirthday dateBirthday = new DateBirthday();
                dateBirthday.setDate(localDate.getDate());
                dateBirthday.setName(localDate.getName());
                dateBirthday.setPhone(localDate.getPhone());
                dateBirthday.setUniqueId(localDate.getUniqueId());
                dateBirthday.setImageUrl(localDate.getImageUrl());
                firebaseListDates.add(dateBirthday);
            }
        }));
    }

    /**
     * Check downloaded items
     *
     * @param firebaseListDates is list of items from fire base
     */
    private void synchronizeLists(List<DateBirthday> firebaseListDates) {
        List<DateBirthday> localListDates = contactQuery.list();
        DisposableManager.add(Observable.fromIterable(firebaseListDates).doOnError(throwable -> {
            firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.refresh_error));
            Log.d(TAG, "Insert to Local DB Error: " + throwable.getMessage());
        }).doOnComplete(() ->
                checkLocalList(firebaseListDates)).forEach(firebaseDate -> {
            boolean insertToDB = true;
            for (int i = 0; i < localListDates.size(); i++) {
                DateBirthday localDate = localListDates.get(i);
                if (firebaseDate.getUniqueId().equals(localDate.getUniqueId())) {
                    insertToDB = false;
                    break;
                }
            }
            if (insertToDB) {
                DateBirthday dateBirthday = new DateBirthday();
                dateBirthday.setDate(firebaseDate.getDate());
                dateBirthday.setName(firebaseDate.getName());
                dateBirthday.setPhone(firebaseDate.getPhone());
                dateBirthday.setUniqueId(firebaseDate.getUniqueId());
                dateBirthday.setImageUrl(firebaseDate.getImageUrl());
                dateBirthdayDao.insertOrReplace(dateBirthday);
            }
        }));
    }

    /**
     * Delete item.
     *
     * @param dateBirthday the date birthday
     * @param position     the position
     */
    public void deleteItem(DateBirthday dateBirthday, int position) {
        Log.d(TAG, "deleteItem: " + dateBirthday.getId());
        try {
            firebase.getReference().child("dates").child(dateBirthday.getUniqueId()).removeValue().addOnSuccessListener(task -> {
                dateBirthdayDao.delete(dateBirthday);
                firebaseDatesListFragment.deleteItem(position);
            }).addOnFailureListener(e -> firebaseDatesListFragment.snackBarWithoutAction(firebaseDatesListFragment.getString(R.string.delete_error)));
        } catch (Exception e) {
            firebaseDatesListFragment.snackBarWithoutAction(firebaseDatesListFragment.getString(R.string.delete_error));
            Log.d(TAG, "Delete item Error: " + e.getMessage());
        }
    }

    /**
     * Gets dates list.
     *
     * @return the dates list
     */
    public List<RecyclerInterface> getDatesList() {
        List<DateBirthday> dateBirthdayList = contactQuery.list();
        Observable.fromIterable(dateBirthdayList).toSortedList((o1, o2) -> {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = format.parse(o1.getDate());
                date2 = format.parse(o2.getDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            assert date1 != null;
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);
            cal1.set(Calendar.YEAR, 2000);

            assert date2 != null;
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);
            cal2.set(Calendar.YEAR, 2000);

            return -1 * cal2.getTime().compareTo(cal1.getTime());

        }).subscribe(dateBirthdays -> {
            dateBirthdayList.clear();
            dateBirthdayList.addAll(dateBirthdays);
            dateBirthdayDao.updateInTx(dateBirthdays);
        }, throwable -> {
            firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.refresh_error));
            Log.d(TAG, "Sort List Error: " + throwable.getMessage());
        }).dispose();

        List<RecyclerInterface> recyclerInterfaces = new ArrayList<>();

        int mounth = -1;

        for (int i = 0; i < dateBirthdayList.size(); i++) {
            DateBirthday dateBirthday = dateBirthdayList.get(i);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date date;

            try {
                date = format.parse(dateBirthday.getDate());
            } catch (ParseException e) {
                firebaseDatesListFragment.refreshError(firebaseDatesListFragment.getString(R.string.build_list_error));
                e.printStackTrace();
                break;
            }

            assert date != null;
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            if (mounth == -1) {
                mounth = cal.get(Calendar.MONTH);
                recyclerInterfaces.add(new HeaderItem(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
                recyclerInterfaces.add(dateBirthday);
            } else if (mounth != cal.get(Calendar.MONTH)) {
                mounth = cal.get(Calendar.MONTH);
                recyclerInterfaces.add(new HeaderItem(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
                recyclerInterfaces.add(dateBirthday);
            } else {
                recyclerInterfaces.add(dateBirthday);
            }
        }

        return recyclerInterfaces;
    }
}
