/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.poroshyn.testandroid.di.ComponentFactory;
import com.poroshyn.testandroid.di.component.base.BaseComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;

/**
 * The type Application.
 */
public class Application extends MultiDexApplication {
    private ComponentFactory mDagger;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);

        mDagger = new ComponentFactory(this);
    }

    /**
     * Gets component.
     *
     * @param componentId the component id
     * @return the component
     * @throws ComponentNotInitializedException the component not initialized exception
     */
    public BaseComponent getComponent(int componentId) throws ComponentNotInitializedException {
        return mDagger.getComponent(componentId);
    }
}
