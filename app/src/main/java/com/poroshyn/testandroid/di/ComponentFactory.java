/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di;

import android.app.Application;

import com.poroshyn.testandroid.di.component.base.BaseComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;

/**
 * The type Component factory.
 */
public class ComponentFactory {

    /**
     * The constant MAIN_COMPONENT.
     */
    public static final int MAIN_COMPONENT = 0x0000014D;

    private final DaggerInitializer mDaggerInitializer;

    /**
     * Instantiates a new Component factory.
     *
     * @param app the app
     */
    public ComponentFactory(Application app) {
        mDaggerInitializer = new DaggerInitializer();
        mDaggerInitializer.initAppComponent(app);
    }

    /**
     * Gets component.
     *
     * @param componentId the component id
     * @return the component
     * @throws ComponentNotInitializedException the component not initialized exception
     */
    public BaseComponent getComponent(int componentId) throws ComponentNotInitializedException {
        BaseComponent component = null;
        if (componentId == MAIN_COMPONENT) {
            component = mDaggerInitializer.getMainComponent();
        }
        return component;
    }

    /**
     * Clear component.
     *
     * @param componentId the component id
     */
    public void clearComponent(int componentId) {
        if (componentId == MAIN_COMPONENT) {
            mDaggerInitializer.clearMainComponent();
        }
    }

}
