/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.component.base;

/**
 * The interface Base component.
 */
public interface BaseComponent {
}
