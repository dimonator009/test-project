/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.module;

import android.content.Context;

import com.poroshyn.testandroid.data.firebase.Firebase;
import com.poroshyn.testandroid.data.firebase.IFirebase;
import com.poroshyn.testandroid.di.annotation.PerActivity;
import com.poroshyn.testandroid.di.annotation.PerMainActivity;
import com.poroshyn.testandroid.presenter.impl.AddRecordPresenter;
import com.poroshyn.testandroid.presenter.impl.DatesListPresenter;
import com.poroshyn.testandroid.presenter.impl.FirebaseDatesListPresenter;

import dagger.Module;
import dagger.Provides;

import static com.poroshyn.testandroid.di.module.AppModule.getDaoSession;

/**
 * The type Main module.
 */
@Module
public class MainModule {

    /**
     * Instantiates a new Main module.
     */
    public MainModule() {
    }


    /**
     * Gets firebase.
     *
     * @return the firebase
     */
    @Provides
    @PerActivity
    IFirebase getFirebase() {
        return new Firebase();
    }

    /**
     * Add record presenter add record presenter.
     *
     * @param context the context
     * @return the add record presenter
     */
    @Provides
    @PerMainActivity
    AddRecordPresenter addRecordPresenter(Context context) {
        return new AddRecordPresenter(getDaoSession(context));
    }

    /**
     * Date list presenter dates list presenter.
     *
     * @param context the context
     * @return the dates list presenter
     */
    @Provides
    @PerMainActivity
    DatesListPresenter dateListPresenter(Context context) {
        return new DatesListPresenter(getDaoSession(context));
    }

    /**
     * Firebase dates list presenter firebase dates list presenter.
     *
     * @param context the context
     * @return the firebase dates list presenter
     */
    @Provides
    @PerMainActivity
    FirebaseDatesListPresenter firebaseDatesListPresenter(Context context) {
        return new FirebaseDatesListPresenter(getFirebase(), getDaoSession(context));
    }

}
