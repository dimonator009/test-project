/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.annotation;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The interface Per activity.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}
