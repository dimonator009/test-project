/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.component;

import android.app.Application;

import com.poroshyn.testandroid.di.component.base.BaseComponent;
import com.poroshyn.testandroid.di.module.AppModule;
import com.poroshyn.testandroid.di.module.MainModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * The interface App component.
 */
@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent extends BaseComponent {
    /**
     * Init main component main component.
     *
     * @param mainModule the main module
     * @return the main component
     */
    MainComponent initMainComponent(MainModule mainModule);

    /**
     * Inject app.
     *
     * @param app the app
     */
    void injectApp(Application app);
}
