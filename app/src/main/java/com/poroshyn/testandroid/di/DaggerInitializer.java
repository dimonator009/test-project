/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di;

import android.app.Application;

import com.poroshyn.testandroid.di.component.AppComponent;
import com.poroshyn.testandroid.di.component.DaggerAppComponent;
import com.poroshyn.testandroid.di.component.MainComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;
import com.poroshyn.testandroid.di.module.AppModule;
import com.poroshyn.testandroid.di.module.MainModule;

/**
 * The type Dagger initializer.
 */
class DaggerInitializer {
    private AppComponent mAppComponent;
    private MainComponent mMainComponent;

    /**
     * Init app component.
     *
     * @param app the app
     */
    void initAppComponent(Application app) {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(app))
                .build();
        mAppComponent.injectApp(app);
    }

    /**
     * Gets main component.
     *
     * @return the main component
     * @throws ComponentNotInitializedException the component not initialized exception
     */
    MainComponent getMainComponent() throws ComponentNotInitializedException {
        if (null == mAppComponent) {
            throw new ComponentNotInitializedException("Application component mustn't be null!");
        }
        if (null == mMainComponent) {
            mMainComponent = mAppComponent.initMainComponent(new MainModule());
        }
        return mMainComponent;
    }

    /**
     * Clear main component.
     */
    void clearMainComponent() {
        mMainComponent = null;
    }

}
