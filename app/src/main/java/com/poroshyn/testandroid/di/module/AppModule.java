/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.module;

import android.content.Context;

import androidx.annotation.NonNull;

import com.poroshyn.testandroid.data.DaoMaster;
import com.poroshyn.testandroid.data.DaoSession;
import com.poroshyn.testandroid.data.greenDao.DBOpenHelper;
import com.poroshyn.testandroid.di.annotation.PerActivity;

import org.greenrobot.greendao.database.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * The type App module.
 */
@Module
public class AppModule {
    private final Context mApplicationContext;

    /**
     * Instantiates a new App module.
     *
     * @param c the c
     */
    public AppModule(@NonNull Context c) {
        this.mApplicationContext = c;
    }

    /**
     * Gets dao session.
     *
     * @param context the context
     * @return the dao session
     */
    @Provides
    @PerActivity
    static DaoSession getDaoSession(Context context) {
        DBOpenHelper helper = new DBOpenHelper(context, "mydatesbithday-db");
        Database db = helper.getWritableDb();
        return new DaoMaster(db).newSession();
    }

    /**
     * Provide context context.
     *
     * @return the context
     */
    @Provides
    @Singleton
    Context provideContext() {
        return mApplicationContext;
    }
}
