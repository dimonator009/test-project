/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.component;

import com.poroshyn.testandroid.di.annotation.PerMainActivity;
import com.poroshyn.testandroid.di.component.base.BaseComponent;
import com.poroshyn.testandroid.di.module.MainModule;
import com.poroshyn.testandroid.view.fragment.impl.AddRecordFragment;
import com.poroshyn.testandroid.view.fragment.impl.DatesListFragment;
import com.poroshyn.testandroid.view.fragment.impl.FirebaseDatesListFragment;

import dagger.Subcomponent;

/**
 * The interface Main component.
 */
@Subcomponent(modules = {MainModule.class})
@PerMainActivity
public interface MainComponent extends BaseComponent {

    /**
     * Inject.
     *
     * @param f1 the f 1
     */
    void inject(AddRecordFragment f1);

    /**
     * Inject.
     *
     * @param f2 the f 2
     */
    void inject(DatesListFragment f2);

    /**
     * Inject.
     *
     * @param f3 the f 3
     */
    void inject(FirebaseDatesListFragment f3);
}
