/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.exception;

/**
 * The type Incompatible class type.
 */
public class IncompatibleClassType extends Exception {

    /**
     * Instantiates a new Incompatible class type.
     *
     * @param message the message
     */
    public IncompatibleClassType(String message) {
        super(message);
    }
}
