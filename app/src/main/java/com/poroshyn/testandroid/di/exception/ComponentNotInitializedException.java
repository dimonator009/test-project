/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.di.exception;

/**
 * The type Component not initialized exception.
 */
public class ComponentNotInitializedException extends Exception {
    /**
     * Instantiates a new Component not initialized exception.
     *
     * @param message the message
     */
    public ComponentNotInitializedException(String message) {
        super(message);
    }
}
