/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.adapter.viewHolder;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.data.HeaderItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The type Header list view holder.
 */
public class HeaderListViewHolder extends RecyclerView.ViewHolder {
    /**
     * The Month text.
     */
    @BindView(R.id.monthText)
    AppCompatTextView monthText;

    /**
     * Instantiates a new Header list view holder.
     *
     * @param view the view
     */
    public HeaderListViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    /**
     * Bind.
     *
     * @param headerItem the header item
     */
    public void bind(HeaderItem headerItem) {
        monthText.setText(headerItem.getMonth());
    }
}
