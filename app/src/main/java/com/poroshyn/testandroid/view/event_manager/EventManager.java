/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.event_manager;

import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.view.event_manager.callback.CallPhoneNumber;
import com.poroshyn.testandroid.view.event_manager.callback.DeleteItem;
import com.poroshyn.testandroid.view.event_manager.callback.RefreshList;
import com.poroshyn.testandroid.view.event_manager.callback.ShowAddRecordFragment;
import com.poroshyn.testandroid.view.event_manager.callback.ShowDatesListFragment;

import org.greenrobot.eventbus.EventBus;

/**
 * The type Event manager.
 */
public class EventManager {

    private EventManager() {

    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static EventManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * Delete item.
     *
     * @param dateBirthday the date birthday
     * @param position     the position
     */
    public void deleteItem(DateBirthday dateBirthday, int position) {
        EventBus.getDefault().post(new DeleteItem(dateBirthday, position));
    }

    /**
     * Show add record fragment.
     *
     * @param contact the contact
     */
    public void showAddRecordFragment(DateBirthday contact) {
        EventBus.getDefault().post(new ShowAddRecordFragment(contact));
    }

    /**
     * Show date list fragment.
     */
    public void showDateListFragment() {
        EventBus.getDefault().post(new ShowDatesListFragment());
    }

    /**
     * Refresh list.
     */
    public void refreshList() {
        EventBus.getDefault().post(new RefreshList());
    }

    /**
     * Phone number.
     *
     * @param contact the contact
     */
    public void phoneNumber(DateBirthday contact) {
        EventBus.getDefault().post(new CallPhoneNumber(contact));
    }

    private static class SingletonHolder {
        /**
         * The Instance.
         */
        static final EventManager INSTANCE = new EventManager();
    }

}
