/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.activity.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.poroshyn.testandroid.view.fragment.impl.factory.FragmentFactory;

/**
 * The type Base activity.
 */
public abstract class BaseActivity extends AppCompatActivity {
    /**
     * Instantiates a new Base activity.
     */
    protected BaseActivity() {
    }

    /**
     * Show fragment.
     *
     * @param activity     the activity
     * @param nextFragment the next fragment
     * @param containerId  the container id
     * @param args         the args
     * @param addToBack    the add to back
     */
    public static void showFragment(FragmentActivity activity,
                                    Fragment nextFragment,
                                    int containerId,
                                    Bundle args,
                                    boolean addToBack) {
        FragmentFactory.getInstance().loadFragment(activity, nextFragment, containerId, addToBack, args);
    }

    /**
     * Run activity.
     *
     * @param nextActivity the next activity
     * @param clearStack   the clear stack
     */
    public void runActivity(Class nextActivity, boolean clearStack) {
        Intent i = new Intent(this, nextActivity);
        if (clearStack) i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

}
