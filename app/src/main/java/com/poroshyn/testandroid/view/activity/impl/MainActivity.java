/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.activity.impl;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.view.activity.base.BaseActivity;
import com.poroshyn.testandroid.view.event_manager.EventManager;
import com.poroshyn.testandroid.view.event_manager.callback.ShowAddRecordFragment;
import com.poroshyn.testandroid.view.event_manager.callback.ShowDatesListFragment;
import com.poroshyn.testandroid.view.fragment.impl.factory.FragmentFactory;
import com.poroshyn.testandroid.view.fragment.impl.factory.FragmentId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The type Main activity.
 */
public class MainActivity extends BaseActivity {

    /**
     * The Toolbar.
     */
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    /**
     * The Content frame.
     */
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    /**
     * The Drawer layout.
     */
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    /**
     * The Nav view.
     */
    @BindView(R.id.nav_view)
    NavigationView navView;
    /**
     * The Main frame.
     */
    @BindView(R.id.main_frame)
    FrameLayout mainFrame;
    /**
     * The Is can refresh.
     */
    boolean isCanRefresh;
    /**
     * The Is back button.
     */
    boolean isBackButton;
    /**
     * The Actionbar.
     */
    ActionBar actionbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        isBackButton = false;
        isCanRefresh = false;
        showFragment(this, FragmentFactory.getInstance().createFragment(
                FragmentId.DATES_LIST_FRAGMENT), mainFrame.getId(), null, false);

        actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        navView.setCheckedItem(R.id.navList);
        navView.setNavigationItemSelectedListener(
                menuItem -> {
                    drawerLayout.closeDrawers();
                    switch (menuItem.getItemId()) {
                        case R.id.navAddRecord:
                            isCanRefresh = false;
                            isBackButton = false;
                            invalidateOptionsMenu();
                            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
                            new Handler().postDelayed(() -> showFragment(this, FragmentFactory.getInstance().createFragment(
                                    FragmentId.ADD_RECORD_FRAGMENT), mainFrame.getId(), null, false), 300);
                            break;
                        case R.id.navList:
                            isCanRefresh = false;
                            isBackButton = false;
                            invalidateOptionsMenu();
                            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
                            new Handler().postDelayed(() -> showFragment(this, FragmentFactory.getInstance().createFragment(
                                    FragmentId.DATES_LIST_FRAGMENT), mainFrame.getId(), null, false), 300);
                            break;
                        case R.id.navFirebaseList:
                            isCanRefresh = true;
                            isBackButton = false;
                            invalidateOptionsMenu();
                            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
                            new Handler().postDelayed(() -> showFragment(this, FragmentFactory.getInstance().createFragment(
                                    FragmentId.FIREBASE_DATES_LIST_FRAGMENT), mainFrame.getId(), null, false), 300);
                            break;
                    }
                    return true;
                });
    }

    /**
     * Show add record fragment.
     *
     * @param showAddRecordFragment the show add record fragment
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showAddRecordFragment(ShowAddRecordFragment showAddRecordFragment) {
        isCanRefresh = false;
        isBackButton = true;
        invalidateOptionsMenu();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        if (showAddRecordFragment.getDateBirthday() != null) {
            Bundle args = new Bundle();
            args.putLong("databaseId", showAddRecordFragment.getDateBirthday().getId());
            showFragment(this, FragmentFactory.getInstance().createFragment(
                    FragmentId.ADD_RECORD_FRAGMENT), mainFrame.getId(), args, true);
        } else {
            showFragment(this, FragmentFactory.getInstance().createFragment(
                    FragmentId.ADD_RECORD_FRAGMENT), mainFrame.getId(), null, true);
        }
    }

    /**
     * Show dates list fragment.
     *
     * @param showDatesListFragment the show dates list fragment
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showDatesListFragment(ShowDatesListFragment showDatesListFragment) {
        navView.setCheckedItem(R.id.navList);
        isCanRefresh = false;
        isBackButton = false;
        invalidateOptionsMenu();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        showFragment(this, FragmentFactory.getInstance().createFragment(
                FragmentId.DATES_LIST_FRAGMENT), mainFrame.getId(), null, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isBackButton)
                    onBackPressed();
                else
                    drawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_refresh:
                EventManager.getInstance().refreshList();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem refresh = menu.findItem(R.id.action_refresh);
        if (!isCanRefresh) {
            refresh.setVisible(false);
        } else {
            refresh.setVisible(true);
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isBackButton) {
            isBackButton = false;
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
    }
}
