/*
 * Developed by Dmitriy Poroshyn on 4/22/19 10:45 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.event_manager.callback;

import com.poroshyn.testandroid.data.DateBirthday;

/**
 * The type Phone number.
 */
public class CallPhoneNumber {
    private DateBirthday dateBirthday;

    /**
     * Instantiates a new Phone number.
     *
     * @param dateBirthday the date birthday
     */
    public CallPhoneNumber(DateBirthday dateBirthday) {
        this.dateBirthday = dateBirthday;
    }

    /**
     * Gets date birthday.
     *
     * @return the date birthday
     */
    public DateBirthday getDateBirthday() {
        return dateBirthday;
    }
}
