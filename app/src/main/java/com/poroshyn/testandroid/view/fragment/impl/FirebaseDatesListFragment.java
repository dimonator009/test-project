/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.fragment.impl;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.di.ComponentFactory;
import com.poroshyn.testandroid.di.component.MainComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;
import com.poroshyn.testandroid.presenter.impl.FirebaseDatesListPresenter;
import com.poroshyn.testandroid.view.activity.impl.MainActivity;
import com.poroshyn.testandroid.view.adapter.DatesListAdapter;
import com.poroshyn.testandroid.view.customView.StickHeaderItemDecoration;
import com.poroshyn.testandroid.view.event_manager.callback.CallPhoneNumber;
import com.poroshyn.testandroid.view.event_manager.EventManager;
import com.poroshyn.testandroid.view.event_manager.callback.DeleteItem;
import com.poroshyn.testandroid.view.event_manager.callback.RefreshList;
import com.poroshyn.testandroid.view.fragment.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * The type Firebase dates list fragment.
 */
public class FirebaseDatesListFragment extends BaseFragment {


    private static int RESULT_PHONE_PERMISSION = 6000;
    /**
     * The Firebase dates list presenter.
     */
    @Inject
    FirebaseDatesListPresenter firebaseDatesListPresenter;

    /**
     * The Date list.
     */
    @BindView(R.id.dateList)
    RecyclerView dateList;
    /**
     * The Add record list.
     */
    @BindView(R.id.addRecordList)
    FloatingActionButton addRecordList;
    /**
     * The Refresh layout.
     */
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    /**
     * The Empty image.
     */
    @BindView(R.id.emptyImage)
    LinearLayout emptyImage;

    /**
     * The Adapter.
     */
    private DatesListAdapter adapter;
    private String phoneToCall;

    @Override
    protected int setLayoutRes() {
        return R.layout.dates_list_fragment;
    }

    @Override
    protected void inject() {
        try {
            ((MainComponent) getComponent(ComponentFactory.MAIN_COMPONENT)).inject(this);
        } catch (ComponentNotInitializedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void init(View v) {
        firebaseDatesListPresenter.setView(this);

        Objects.requireNonNull(((MainActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar())
                .setTitle(getActivity().getString(R.string.cloud_list));

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        dateList.setLayoutManager(layoutManager);

        initRecycler();
        refreshLayout.setRefreshing(true);
        firebaseDatesListPresenter.getDatesListFirebase();

        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80,
                getResources().getDisplayMetrics());

        dateList.setClipToPadding(false);
        dateList.setPadding(0, 0, 0, padding);

        checkIfListEmpty();

        refreshLayout.setOnRefreshListener(() -> firebaseDatesListPresenter.getDatesListFirebase());
    }

    /**
     * Show is empty picture
     */
    private void checkIfListEmpty() {
        if (firebaseDatesListPresenter.getDatesList().isEmpty()) {
            emptyImage.setVisibility(View.VISIBLE);
        } else {
            emptyImage.setVisibility(View.GONE);
        }
    }

    /**
     * Delete item.
     *
     * @param position the position
     */
    public void deleteItem(int position) {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        checkIfListEmpty();
        adapter.getDateBirthdayList().remove(position);
        adapter.notifyItemRemoved(position);
        adapter.setDateBirthdayList(firebaseDatesListPresenter.getDatesList());
        adapter.notifyDataSetChanged();
        snackBarWithoutAction(getString(R.string.deleted));
    }

    /**
     * Init recycler.
     */
    private void initRecycler() {
        checkIfListEmpty();
        adapter = new DatesListAdapter(firebaseDatesListPresenter.getDatesList());
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom);
        dateList.setLayoutAnimation(animation);
        dateList.setAdapter(adapter);
        dateList.addItemDecoration(new StickHeaderItemDecoration(adapter));
    }

    /**
     * Add dates list.
     */
    public void addDatesList() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getView() != null) {
            Snackbar mySnackbar = Snackbar.make(getView(),
                    getString(R.string.list_refresh), 3000);
            mySnackbar.show();
        }
        initRecycler();
    }

    /**
     * Snack bar without action.
     *
     * @param message the message
     */
    public void snackBarWithoutAction(String message) {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getView() != null) {
            Snackbar mySnackbar = Snackbar.make(getView(),
                    message, Snackbar.LENGTH_LONG);
            mySnackbar.show();
        }
    }

    /**
     * Refresh error.
     *
     * @param error the error
     */
    public void refreshError(String error) {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getView() != null) {
            Snackbar mySnackbar = Snackbar.make(getView(),
                    error, Snackbar.LENGTH_LONG);
            mySnackbar.setAction(getString(R.string.retry), v -> {
                refreshLayout.setRefreshing(true);
                firebaseDatesListPresenter.getDatesListFirebase();
            });
            mySnackbar.show();
        }
    }

    /**
     * On view clicked.
     */
    @OnClick(R.id.addRecordList)
    public void onViewClicked() {
        EventManager.getInstance().showAddRecordFragment(null);
    }

    /**
     * Refresh list.
     *
     * @param refreshList the refresh list
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshList(RefreshList refreshList) {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
            firebaseDatesListPresenter.getDatesListFirebase();
        }
    }

    /**
     * Phone number.
     *
     * @param callPhoneNumber the call phone number
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void phoneNumber(CallPhoneNumber callPhoneNumber) {
        if (!refreshLayout.isRefreshing()) {
            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[]{Manifest.permission.CALL_PHONE},
                        RESULT_PHONE_PERMISSION);
                phoneToCall = callPhoneNumber.getDateBirthday().getPhone();
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callPhoneNumber.getDateBirthday().getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.please_wait_for_refresh), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Open call activity
     *
     * @param phone is user phone
     */
    private void callNumber(String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Delete item.
     *
     * @param deleteItem the delete item
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void deleteItem(DeleteItem deleteItem) {
        if (!refreshLayout.isRefreshing()) {
            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setMessage(getString(R.string.delete_confirmation));
            alertDialog.setTitle(getString(R.string.warning));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, (getString(R.string.yes)),
                    (dialog, which) -> {
                        refreshLayout.setRefreshing(true);
                        firebaseDatesListPresenter.deleteItem(deleteItem.getDateBirthday(), deleteItem.getPosition());
                        alertDialog.dismiss();
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, (getString(R.string.no)),
                    (dialog, which) -> alertDialog.dismiss());
            alertDialog.show();
        } else {
            Toast.makeText(getActivity(), getString(R.string.please_wait_for_refresh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RESULT_PHONE_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            callNumber(phoneToCall);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        firebaseDatesListPresenter.unbindView();
        EventBus.getDefault().unregister(this);
    }


}
