/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.fragment.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.poroshyn.testandroid.Application;
import com.poroshyn.testandroid.di.component.base.BaseComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;

import java.util.Objects;

import butterknife.ButterKnife;

/**
 * The type Base fragment.
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(setLayoutRes(), container, false);
        ButterKnife.bind(this, v);
        init(v);
        return v;
    }

    /**
     * Sets layout res.
     *
     * @return the layout res
     */
    protected abstract int setLayoutRes();

    /**
     * Inject.
     */
    protected abstract void inject();

    /**
     * Init.
     *
     * @param v the v
     */
    protected abstract void init(View v);

    /**
     * Gets component.
     *
     * @param componentType the component type
     * @return the component
     * @throws ComponentNotInitializedException the component not initialized exception
     */
    protected BaseComponent getComponent(int componentType) throws ComponentNotInitializedException {
        return ((Application) Objects.requireNonNull(getActivity()).getApplication()).getComponent(componentType);
    }

}
