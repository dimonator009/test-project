/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.adapter.viewHolder;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.view.event_manager.EventManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The type Date list view holder.
 */
public class DateListViewHolder extends RecyclerView.ViewHolder {

    /**
     * The Day.
     */
    @BindView(R.id.day)
    AppCompatTextView day;
    /**
     * The Photo.
     */
    @BindView(R.id.photo)
    AppCompatImageView photo;
    /**
     * The Name.
     */
    @BindView(R.id.name)
    AppCompatTextView name;
    /**
     * The Date.
     */
    @BindView(R.id.date)
    AppCompatTextView date;
    /**
     * The Delete.
     */
    @BindView(R.id.delete)
    AppCompatImageView delete;


    /**
     * The Phone.
     */
    @BindView(R.id.phone)
    AppCompatImageView phone;

    /**
     * The View.
     */
    @BindView(R.id.listItem)
    CardView view;

    /**
     * Instantiates a new Date list view holder.
     *
     * @param view the view
     */
    public DateListViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    /**
     * Bind.
     *
     * @param contact  the contact
     * @param position the position
     */
    public void bind(DateBirthday contact, int position) {
        Glide.with(Objects.requireNonNull(itemView.getContext()))
                .load(contact.getImageUrl())
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        contact.setImageUrl("");
                        try {
                            contact.update();
                        } catch (Exception ignored) {

                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .placeholder(R.drawable.ic_empy_photo)
                .into(photo);

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date dateBirthday = null;
        try {
            dateBirthday = format.parse(contact.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        view.setOnClickListener(v -> EventManager.getInstance().showAddRecordFragment(contact));
        if(!contact.getPhone().isEmpty())
            phone.setVisibility(View.VISIBLE);
        phone.setOnClickListener(v -> EventManager.getInstance().phoneNumber(contact));

        assert dateBirthday != null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateBirthday);

        String dayString = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

        day.setText(dayString);

        name.setText(contact.getName());
        date.setText(contact.getDate());

        delete.setOnClickListener(v -> EventManager.getInstance().deleteItem(contact, position));
    }
}
