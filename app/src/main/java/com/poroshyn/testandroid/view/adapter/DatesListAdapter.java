/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.data.HeaderItem;
import com.poroshyn.testandroid.data.RecyclerInterface;
import com.poroshyn.testandroid.view.adapter.viewHolder.DateListViewHolder;
import com.poroshyn.testandroid.view.adapter.viewHolder.HeaderListViewHolder;
import com.poroshyn.testandroid.view.customView.StickHeaderItemDecoration;

import java.util.List;

/**
 * The type Dates list adapter.
 */
public class DatesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickHeaderItemDecoration.StickyHeaderInterface {

    private static final int HEADER = 1;
    private List<RecyclerInterface> mDateBirthdayList;


    /**
     * Instantiates a new Dates list adapter.
     *
     * @param mDateBirthdayList the m date birthday list
     */
    public DatesListAdapter(List<RecyclerInterface> mDateBirthdayList) {
        this.mDateBirthdayList = mDateBirthdayList;
    }

    /**
     * Gets date birthday list.
     *
     * @return the date birthday list
     */
    public List<RecyclerInterface> getDateBirthdayList() {
        return mDateBirthdayList;
    }

    /**
     * Sets date birthday list.
     *
     * @param mDateBirthdayList the m date birthday list
     */
    public void setDateBirthdayList(List<RecyclerInterface> mDateBirthdayList) {
        this.mDateBirthdayList = mDateBirthdayList;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDateBirthdayList.get(position).getClass() == HeaderItem.class) {
            return 1;
        } else {
            return 0;

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        if (viewType == HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item, parent, false);
            viewHolder = new HeaderListViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_list_item, parent, false);
            viewHolder = new DateListViewHolder(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == HEADER) {
            HeaderItem dateBirthday = (HeaderItem) mDateBirthdayList.get(position);
            HeaderListViewHolder dateListViewHolder = (HeaderListViewHolder) holder;
            dateListViewHolder.bind(dateBirthday);
        } else {
            DateBirthday dateBirthday = (DateBirthday) mDateBirthdayList.get(position);
            DateListViewHolder dateListViewHolder = (DateListViewHolder) holder;
            dateListViewHolder.bind(dateBirthday, position);
        }
    }

    @Override
    public int getItemCount() {
        return mDateBirthdayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    @Override
    public int getHeaderLayout(int headerPosition) {
        return R.layout.header_item;
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {

        HeaderListViewHolder viewHolder;
        viewHolder = new HeaderListViewHolder(header);

        if (headerPosition < mDateBirthdayList.size()) {
            HeaderItem dateBirthday = (HeaderItem) mDateBirthdayList.get(headerPosition);
            viewHolder.bind(dateBirthday);
        }
    }

    @Override
    public boolean isHeader(int itemPosition) {
        if (itemPosition < mDateBirthdayList.size() && itemPosition > 0)
            return mDateBirthdayList.get(itemPosition).getClass() == HeaderItem.class;
        else
            return false;
    }
}
