/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.event_manager.callback;

import com.poroshyn.testandroid.data.DateBirthday;

/**
 * The type Delete item.
 */
public class DeleteItem {
    private DateBirthday dateBirthday;
    private int position;

    /**
     * Instantiates a new Delete item.
     *
     * @param dateBirthday the date birthday
     * @param position     the position
     */
    public DeleteItem(DateBirthday dateBirthday, int position) {
        this.dateBirthday = dateBirthday;
        this.position = position;
    }

    /**
     * Gets position.
     *
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Gets date birthday.
     *
     * @return the date birthday
     */
    public DateBirthday getDateBirthday() {
        return dateBirthday;
    }
}
