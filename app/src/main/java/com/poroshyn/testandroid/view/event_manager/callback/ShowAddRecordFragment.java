/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.event_manager.callback;

import com.poroshyn.testandroid.data.DateBirthday;

/**
 * The type Show add record fragment.
 */
public class ShowAddRecordFragment {

    private DateBirthday dateBirthday;

    /**
     * Instantiates a new Show add record fragment.
     *
     * @param dateBirthday the date birthday
     */
    public ShowAddRecordFragment(DateBirthday dateBirthday) {
        this.dateBirthday = dateBirthday;
    }

    /**
     * Gets date birthday.
     *
     * @return the date birthday
     */
    public DateBirthday getDateBirthday() {
        return dateBirthday;
    }
}
