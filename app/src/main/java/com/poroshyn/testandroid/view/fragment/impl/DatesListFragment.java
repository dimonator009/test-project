/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.fragment.impl;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.di.ComponentFactory;
import com.poroshyn.testandroid.di.component.MainComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;
import com.poroshyn.testandroid.presenter.impl.DatesListPresenter;
import com.poroshyn.testandroid.view.activity.impl.MainActivity;
import com.poroshyn.testandroid.view.adapter.DatesListAdapter;
import com.poroshyn.testandroid.view.customView.StickHeaderItemDecoration;
import com.poroshyn.testandroid.view.event_manager.callback.CallPhoneNumber;
import com.poroshyn.testandroid.view.event_manager.EventManager;
import com.poroshyn.testandroid.view.event_manager.callback.DeleteItem;
import com.poroshyn.testandroid.view.fragment.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * The type Dates list fragment.
 */
public class DatesListFragment extends BaseFragment {

    private static int RESULT_PHONE_PERMISSION = 6000;
    /**
     * The Dates list presenter.
     */
    @Inject
    DatesListPresenter datesListPresenter;
    /**
     * The Date list.
     */
    @BindView(R.id.dateList)
    RecyclerView dateList;
    /**
     * The Refresh layout.
     */
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    /**
     * The Empty image.
     */
    @BindView(R.id.emptyImage)
    LinearLayout emptyImage;
    /**
     * The Adapter.
     */
    private DatesListAdapter adapter;
    private String phoneToCall;

    @Override
    protected int setLayoutRes() {
        return R.layout.dates_list_fragment;
    }

    @Override
    protected void inject() {
        try {
            ((MainComponent) getComponent(ComponentFactory.MAIN_COMPONENT)).inject(this);
        } catch (ComponentNotInitializedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void init(View v) {
        datesListPresenter.setView(this);

        Objects.requireNonNull(((MainActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar())
                .setTitle(getActivity().getString(R.string.record_list));

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        dateList.setLayoutManager(layoutManager);
        refreshLayout.setEnabled(false);

        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80,
                getResources().getDisplayMetrics());

        dateList.setClipToPadding(false);
        dateList.setPadding(0, 0, 0, padding);

        checkIfListEmpty();

        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
        initRecycler();
    }


    /**
     * Delete item.
     *
     * @param deleteItem the delete item
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void deleteItem(DeleteItem deleteItem) {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setMessage(getString(R.string.delete_confirmation));
        alertDialog.setTitle(getString(R.string.warning));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, (getString(R.string.yes)),
                (dialog, which) -> {
                    datesListPresenter.deleteItem(deleteItem.getDateBirthday(), deleteItem.getPosition());
                    alertDialog.dismiss();
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, (getString(R.string.no)),
                (dialog, which) -> alertDialog.dismiss());
        alertDialog.show();
    }

    private void checkIfListEmpty() {
        if (datesListPresenter.getDatesList().isEmpty()) {
            emptyImage.setVisibility(View.VISIBLE);
        } else {
            emptyImage.setVisibility(View.GONE);
        }
    }

    /**
     * Delete item.
     *
     * @param position the position
     */
    public void deleteItem(int position) {
        adapter.getDateBirthdayList().remove(position);
        adapter.notifyItemRemoved(position);
        adapter.setDateBirthdayList(datesListPresenter.getDatesList());
        adapter.notifyDataSetChanged();
        checkIfListEmpty();
        snackBarWithoutAction(getString(R.string.deleted));
    }

    /**
     * Snack bar without action.
     *
     * @param message the message
     */
    public void snackBarWithoutAction(String message) {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getView() != null) {
            Snackbar mySnackbar = Snackbar.make(getView(),
                    message, Snackbar.LENGTH_LONG);
            mySnackbar.show();
        }
    }

    /**
     * Init recycler.
     */
    private void initRecycler() {
        checkIfListEmpty();
        adapter = new DatesListAdapter(datesListPresenter.getDatesList());
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom);
        dateList.setLayoutAnimation(animation);
        dateList.setAdapter(adapter);
        dateList.addItemDecoration(new StickHeaderItemDecoration(adapter));
    }

    /**
     * Phone number.
     *
     * @param callPhoneNumber the call phone number
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void phoneNumber(CallPhoneNumber callPhoneNumber) {
        if (!refreshLayout.isRefreshing()) {
            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[]{Manifest.permission.CALL_PHONE},
                        RESULT_PHONE_PERMISSION);
                phoneToCall = callPhoneNumber.getDateBirthday().getPhone();
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callPhoneNumber.getDateBirthday().getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.please_wait_for_refresh), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Open call activity
     *
     * @param phone is user phone
     */
    private void callNumber(String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RESULT_PHONE_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            callNumber(phoneToCall);
        }
    }

    /**
     * On view clicked.
     */
    @OnClick(R.id.addRecordList)
    public void onViewClicked() {
        EventManager.getInstance().showAddRecordFragment(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        datesListPresenter.unbindView();
        EventBus.getDefault().unregister(this);
    }
}
