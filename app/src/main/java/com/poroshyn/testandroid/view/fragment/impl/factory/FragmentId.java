/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.fragment.impl.factory;

/**
 * The type Fragment id.
 */
public class FragmentId {
    /**
     * The constant ADD_RECORD_FRAGMENT.
     */
    public static final int ADD_RECORD_FRAGMENT = 0x00000001;
    /**
     * The constant DATES_LIST_FRAGMENT.
     */
    public static final int DATES_LIST_FRAGMENT = 0x00000002;
    /**
     * The constant FIREBASE_DATES_LIST_FRAGMENT.
     */
    public static final int FIREBASE_DATES_LIST_FRAGMENT = 0x00000003;

}
