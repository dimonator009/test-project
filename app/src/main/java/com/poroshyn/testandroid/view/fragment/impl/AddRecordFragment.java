/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.fragment.impl;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding3.widget.RxTextView;
import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.data.DateBirthday;
import com.poroshyn.testandroid.di.ComponentFactory;
import com.poroshyn.testandroid.di.component.MainComponent;
import com.poroshyn.testandroid.di.exception.ComponentNotInitializedException;
import com.poroshyn.testandroid.presenter.impl.AddRecordPresenter;
import com.poroshyn.testandroid.view.activity.impl.MainActivity;
import com.poroshyn.testandroid.view.event_manager.EventManager;
import com.poroshyn.testandroid.view.fragment.base.BaseFragment;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.util.Calendar;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

import static android.app.Activity.RESULT_OK;

/**
 * The type Add record fragment.
 */
public class AddRecordFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {


    private static int RESULT_LOAD_IMAGE = 1000;
    private static int RESULT_TAKE_PHOTO = 3000;
    private static int RESULT_READ_EXTERNAL_STORAGE = 2000;
    private static int RESULT_CAMERA_PERMISSION = 4000;
    private static int RESULT_WRITE_EXTERNAL_STORAGE = 5000;


    /**
     * The Add record presenter.
     */
    @Inject
    AddRecordPresenter addRecordPresenter;
    /**
     * The Photo.
     */
    @BindView(R.id.photo)
    AppCompatImageView photo;
    /**
     * The Name.
     */
    @BindView(R.id.name)
    TextInputEditText name;
    /**
     * The Birthday.
     */
    @BindView(R.id.birthday)
    TextInputEditText birthday;
    /**
     * The Phone.
     */
    @BindView(R.id.phone)
    TextInputEditText phone;
    /**
     * The Linear birthday.
     */
    @BindView(R.id.linearBirthday)
    TextInputLayout linearBirthday;
    /**
     * The Add record.
     */
    @BindView(R.id.addRecord)
    FloatingActionButton addRecord;
    private DateBirthday mDateBirthday;
    private boolean isCanSave;
    private boolean isEdit = false;
    private Disposable mDisposableName;
    private Disposable mDisposableDate;
    private Uri returnUri;
    private Long itemId;

    @Override
    protected int setLayoutRes() {
        return R.layout.add_record_fragment;
    }

    @Override
    protected void inject() {
        try {
            ((MainComponent) getComponent(ComponentFactory.MAIN_COMPONENT)).inject(this);
        } catch (ComponentNotInitializedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = this.getArguments();
        if (null != args) {
            itemId = args.getLong("databaseId");
            DateBirthday dateBirthday = addRecordPresenter.getFromDB(itemId);
            if (dateBirthday != null) {
                mDateBirthday = dateBirthday;
            }
        }
    }

    @Override
    protected void init(View v) {
        addRecordPresenter.setView(this);

        Objects.requireNonNull(((MainActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar())
                .setTitle(getActivity().getString(R.string.add_record));

        if (mDateBirthday != null) {
            Log.d("TAG", "init: ");
            name.setText(mDateBirthday.getName());
            birthday.setText(mDateBirthday.getDate());
            phone.setText(mDateBirthday.getPhone());
            loadPhoto(mDateBirthday.getImageUrl());
            Objects.requireNonNull(((MainActivity) Objects.requireNonNull(getActivity()))
                    .getSupportActionBar())
                    .setTitle(getActivity().getString(R.string.edit_item));
            addRecord.setImageDrawable(getResources().getDrawable(R.drawable.ic_save));
            isEdit = true;
        }

        mDisposableName = RxTextView.textChanges(name).subscribe(textViewAfterTextChangeEvent -> checkRuntimeFields());
        mDisposableDate = RxTextView.textChanges(birthday).subscribe(textViewAfterTextChangeEvent -> checkRuntimeFields());
    }

    /**
     * Check text field
     */
    private void checkRuntimeFields() {
        if (Objects.requireNonNull(name.getText()).toString().isEmpty() |
                Objects.requireNonNull(birthday.getText()).toString().isEmpty()) {
            addRecord.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey)));
            isCanSave = false;
        } else {
            addRecord.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            isCanSave = true;
        }
    }

    /**
     * Show dialog to choose type
     */
    private void showDialog() {
        CharSequence[] options = new CharSequence[]{getString(R.string.open_camera), getString(R.string.choose_from_gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.upload_photo));
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    openCamera();
                    break;
                case 1:
                    openGallery();
                    break;
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        addRecordPresenter.unbindView();
        if (mDisposableName != null) mDisposableName.dispose();
        if (mDisposableDate != null) mDisposableDate.dispose();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "." + (monthOfYear + 1) + "." + year;
        birthday.setText(date);
    }

    /**
     * Successfully added.
     */
    public void successfullyAdded() {
        Snackbar mySnackbar = Snackbar.make(Objects.requireNonNull(getView()),
                getString(R.string.saved), Snackbar.LENGTH_LONG);
        mySnackbar.setAction(getString(R.string.go_to), v -> EventManager.getInstance().showDateListFragment());
        mySnackbar.show();
    }

    /**
     * Error while adding.
     *
     * @param dateBirthday the date birthday
     */
    public void errorWhileAdding(DateBirthday dateBirthday) {
        Snackbar mySnackbar = Snackbar.make(Objects.requireNonNull(getView()),
                getString(R.string.save_error), Snackbar.LENGTH_LONG);
        mySnackbar.setAction(getString(R.string.retry), v -> addRecordPresenter.addContactToDB(dateBirthday));
        mySnackbar.show();
    }

    /**
     * Error while save.
     *
     * @param dateBirthday the date birthday
     */
    public void errorWhileSave(DateBirthday dateBirthday) {
        Snackbar mySnackbar = Snackbar.make(Objects.requireNonNull(getView()),
                getString(R.string.save_error), Snackbar.LENGTH_LONG);
        mySnackbar.setAction(getString(R.string.retry), v -> addRecordPresenter.updateItemDB(dateBirthday, itemId));
        mySnackbar.show();
    }

    /**
     * On view clicked.
     *
     * @param view the view
     */
    @OnClick({R.id.photo, R.id.birthday, R.id.addRecord})
    void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.photo:
                showDialog();
                break;
            case R.id.birthday:
                openDatePicker();
                break;
            case R.id.addRecord:
                if (isCanSave) {
                    if (isEdit) {
                        DateBirthday dateBirthday = new DateBirthday();
                        dateBirthday.setName(Objects.requireNonNull(name.getText()).toString());
                        dateBirthday.setDate(Objects.requireNonNull(birthday.getText()).toString());
                        dateBirthday.setPhone(Objects.requireNonNull(phone.getText()).toString());
                        dateBirthday.setUniqueId(mDateBirthday.getUniqueId());
                        if (returnUri != null)
                            dateBirthday.setImageUrl(returnUri.toString());
                        else
                            dateBirthday.setImageUrl("");
                        addRecordPresenter.updateItemDB(dateBirthday, itemId);
                    } else {
                        DateBirthday dateBirthday = new DateBirthday();
                        dateBirthday.setDate(Objects.requireNonNull(birthday.getText()).toString());
                        dateBirthday.setName(Objects.requireNonNull(name.getText()).toString());
                        dateBirthday.setPhone(Objects.requireNonNull(phone.getText()).toString());
                        if (returnUri != null)
                            dateBirthday.setImageUrl(returnUri.toString());
                        else
                            dateBirthday.setImageUrl("");
                        addRecordPresenter.addContactToDB(dateBirthday);
                    }
                } else {
                    if (Objects.requireNonNull(name.getText()).toString().isEmpty()) {
                        name.requestFocus();
                    } else if (Objects.requireNonNull(birthday.getText()).toString().isEmpty()) {
                        openDatePicker();
                    }
                }
                break;
        }
    }

    /**
     * Open camera
     */
    private void openCamera() {
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    RESULT_CAMERA_PERMISSION);
        } else {
            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        RESULT_WRITE_EXTERNAL_STORAGE);
            } else {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, generateFileUri());
                startActivityForResult(intent, RESULT_TAKE_PHOTO);
            }
        }
    }

    /**
     * Generate  file uri for camera photo
     *
     * @return uri
     */
    private Uri generateFileUri() {
        File file;
        file = new File(Objects.requireNonNull(getActivity()).getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + "photo_"
                + System.currentTimeMillis() + ".jpg");
        returnUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);

        return FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
    }

    /**
     * Open date picker
     */
    private void openDatePicker() {
        Calendar currentDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                currentDate.get(Calendar.YEAR),
                currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary));
        assert getFragmentManager() != null;
        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    /**
     * Open gallery
     */
    private void openGallery() {
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    RESULT_READ_EXTERNAL_STORAGE);
        } else {
            Intent intent = new Intent(
                    Intent.ACTION_OPEN_DOCUMENT,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, RESULT_LOAD_IMAGE);
        }
    }

    /**
     * Load photo
     *
     * @param uri is photo uri
     */
    private void loadPhoto(String uri) {
        Glide.with(Objects.requireNonNull(getActivity()))
                .load(uri)
                .placeholder(R.drawable.ic_add_a_photo)
                .into(photo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            returnUri = data.getData();
            assert returnUri != null;
            loadPhoto(returnUri.toString());
            final int takeFlags = data.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            try {
                Objects.requireNonNull(getActivity()).getContentResolver().takePersistableUriPermission(returnUri, takeFlags);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == RESULT_TAKE_PHOTO && resultCode == RESULT_OK) {
            loadPhoto(returnUri.toString());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RESULT_READ_EXTERNAL_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openGallery();
        }
        if (requestCode == RESULT_CAMERA_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openCamera();
        }
        if (requestCode == RESULT_WRITE_EXTERNAL_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openCamera();
        }
    }
}
