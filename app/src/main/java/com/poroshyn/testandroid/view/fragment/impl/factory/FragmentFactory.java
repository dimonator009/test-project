/*
 * Developed by Dmitriy Poroshyn on 4/22/19 1:14 AM
 * Copyright (c) 2019. All right reserved
 */

package com.poroshyn.testandroid.view.fragment.impl.factory;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.poroshyn.testandroid.R;
import com.poroshyn.testandroid.view.fragment.base.BaseFragment;
import com.poroshyn.testandroid.view.fragment.impl.AddRecordFragment;
import com.poroshyn.testandroid.view.fragment.impl.DatesListFragment;
import com.poroshyn.testandroid.view.fragment.impl.FirebaseDatesListFragment;

/**
 * The type Fragment factory.
 */
public class FragmentFactory {

    private FragmentFactory() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static FragmentFactory getInstance() {
        return SingletonHolder.sInstance;
    }

    /**
     * Create fragment base fragment.
     *
     * @param fragmentId the fragment id
     * @return the base fragment
     */
    public BaseFragment createFragment(int fragmentId) {
        BaseFragment instance = null;
        switch (fragmentId) {
            case FragmentId.ADD_RECORD_FRAGMENT:
                instance = new AddRecordFragment();
                break;
            case FragmentId.DATES_LIST_FRAGMENT:
                instance = new DatesListFragment();
                break;
            case FragmentId.FIREBASE_DATES_LIST_FRAGMENT:
                instance = new FirebaseDatesListFragment();
                break;
        }
        return instance;
    }

    /**
     * Load fragment.
     *
     * @param activity    the activity
     * @param fragment    the fragment
     * @param containerId the container id
     * @param addToBack   the add to back
     * @param args        the args
     */
    public void loadFragment(FragmentActivity activity,
                             Fragment fragment,
                             int containerId,
                             boolean addToBack,
                             Bundle args) {
        FragmentTransaction fragmentTransaction =
                activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_bottom, R.anim.enter_from_bottom, R.anim.exit_to_right);
        if (addToBack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        } else {
            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if (null != args) {
            fragment.setArguments(args);
        }
        fragmentTransaction.replace(containerId, fragment);
        fragmentTransaction.commit();
    }

    /**
     * The type Singleton holder.
     */
    static class SingletonHolder {
        /**
         * The S instance.
         */
        static final FragmentFactory sInstance = new FragmentFactory();
    }
}
